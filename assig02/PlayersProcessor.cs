using System;
using System.Threading.Tasks;

namespace assig02
{
    public class PlayersProcessor
    {
        private IRepository _repoToUse;

        public PlayersProcessor(IRepository repository) { _repoToUse = repository; }

        public Task<Player> Get(Guid id) { return _repoToUse.Get(id); }

        public Task<Player[]> GetAll() { return _repoToUse.GetAll(); }

        public Task<Player> Create(NewPlayer player)
        {
            Player np = new Player();
            np.Id = Guid.NewGuid();
            np.Name = player.Name;
            return _repoToUse.Create(np);
        }
        public Task<Player> Modify(Guid id, ModifiedPlayer player) { return _repoToUse.Modify(id, player); }
        
        public Task<Player> Delete(Guid id) { return _repoToUse.Delete(id); }
    }
}