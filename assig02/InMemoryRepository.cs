using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace assig02
{
    public class InMemoryRepository : IRepository
    {
        List<Player> dataBase = new List<Player>();
        public async Task<Player> Get(Guid id)
        {
            Player result = null;
            foreach(Player p in dataBase)
            {
                if(p.Id == id)
                {
                    result = p;
                    break;
                }
            }
            return await Task.FromResult<Player>(result);
        }

        public async Task<Player[]> GetAll()
        {
            return await Task.FromResult<Player[]>(dataBase.ToArray());
        }

        public async Task<Player> Create(Player player)
        {
            dataBase.Add(player);
            return await Task.FromResult<Player>(player);
        }

        public async Task<Player> Modify(Guid id, ModifiedPlayer player)
        {
            Player result = null;
            foreach(Player p in dataBase)
            {
                if(p.Id == id)
                {
                    p.Score = player.Score;
                    result = p;
                    break;
                }
            }
            return await Task.FromResult<Player>(result);
        }

        public async Task<Player> Delete(Guid id)
        {
            Player result = null;
            foreach(Player p in dataBase)
            {
                if(p.Id == id)
                {
                    result = p;
                    dataBase.Remove(p);
                    break;
                }
            }
            return await Task.FromResult<Player>(result);
        }
    }
}