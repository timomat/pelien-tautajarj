using System;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace assig04
{
    public class NewItem
    {
        [Range(1, 99, ErrorMessage = "Level must be between 1 and 99")]
        public int Level { get; set; }

        [ItemType]
        public string Type { get; set; }
        
        [DateValid]
        public DateTime CreationTime { get; set; }
    }
}