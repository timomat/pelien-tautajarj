using System;
using System.Collections.Generic;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace assig04
{
    public class DateValidAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if( ((DateTime)value) <= DateTime.Now )
            {
                return ValidationResult.Success;
            }


            return new ValidationResult("Date cannot be in the future.");
        }
    }
}