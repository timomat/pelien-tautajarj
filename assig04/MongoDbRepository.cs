using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;

namespace assig04
{
    public class MongoDbRepository : IRepository
    {
        private readonly IMongoCollection<Player> _collection;
        private readonly IMongoCollection<BsonDocument> _bsonDocumentCollection;

        public MongoDbRepository()
        {
            MongoClient client = new MongoClient("mongodb://localhost:27017");
            IMongoDatabase db = client.GetDatabase("game");
            _collection = db.GetCollection<Player>("players");
            _bsonDocumentCollection = db.GetCollection<BsonDocument>("players");
        }

        public async Task<Player> Get(Guid id)
        {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq("_id", id);
            return await _collection.Find(filter).FirstAsync();
        }
        public async Task<Player[]> GetAll()
        {
            List<Player> players = await _collection.Find(new BsonDocument()).ToListAsync();
            return players.ToArray();
        }
        public async Task<Player> Create(Player player)
        {
            await _collection.InsertOneAsync(player);
            return player;
        }
        public async Task<Player> Modify(Guid id, Player player)
        {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq("_id", id);
            await _collection.ReplaceOneAsync(filter, player);
            return player;
        }
        public async Task<Player> Delete(Guid id)
        {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq("_id", id);
            Player deleted = await _collection.Find(filter).FirstAsync();
            await _collection.DeleteOneAsync(filter);
            return deleted;
        }
        public async Task<Item> GetItem(Guid playerId, Guid itemId)
        {
            //FilterDefinition<Player> filter = Builders<Player>.Filter.ElemMatch(p => p.Items, i => i.Id == itemId);
            //return await _collection.Find(filter).FirstAsync();
            Item result = null;

            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq("_id", playerId);
            Player p = await _collection.Find(filter).FirstAsync();
            foreach(Item i in p.Items)
            {
                if(i.Id == itemId)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }
        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq("_id", playerId);
            Player p = await _collection.Find(filter).FirstAsync();
            return p.Items.ToArray();
        }
        public async Task<Item> CreateItem(Guid playerId, Item item)
        {
            UpdateDefinition<Player> itm = Builders<Player>.Update.Push<Item>(p => p.Items, item);
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq("_id", playerId);
            Player derp = await _collection.FindOneAndUpdateAsync(filter, itm);
            return item;
        }
        public async Task<Item> ModifyItem(Guid playerId, Guid itemId, ModifiedItem item)
        {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq("_id", playerId);
            UpdateDefinition<Player> itm = Builders<Player>.Update.Set("items.{itemId}.type", item.Type).Set("item.{itemId}.level", item.Level);
            await _collection.UpdateOneAsync(filter, itm);
            return null;
        }
        public async Task<Item> DeleteItem(Guid playerId, Guid itemId)
        {
            UpdateDefinition<Player> itm = Builders<Player>.Update.PullFilter(p => p.Items, i => i.Id == itemId);
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq("_id", playerId);
            Player derp = await _collection.FindOneAndUpdateAsync(filter, itm);
            return null;
        }
    }
}