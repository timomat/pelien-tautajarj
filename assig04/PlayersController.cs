using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace assig04
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController
    {
        private readonly PlayersProcessor _procToUse;

        public PlayersController(PlayersProcessor processor) { _procToUse = processor; }

        [HttpGet]
        [Route("~/api/Get/{id}")]
        public Task<Player> Get(Guid id)
        {
            Console.WriteLine("Get called.");
            return _procToUse.Get(id);
        }

        [HttpGet]
        [Route("~/api/GetAll")]
        public Task<Player[]> GetAll()
        {
            Console.WriteLine("GetAll called.");
            return _procToUse.GetAll();
        }

        [HttpPost]
        [Route("~/api/Create")]
        public Task<Player> Create(NewPlayer player)
        {
            Console.WriteLine("Create called.");
            return _procToUse.Create(player);
        }

        [HttpPost]
        [Route("~/api/Modify/{id}")]
        public Task<Player> Modify(Guid id, ModifiedPlayer player)
        {
            Console.WriteLine("Modify called.");
            return _procToUse.Modify(id, player);
        }

        [HttpDelete]
        [Route("~/api/Delete/{id}")]
        public Task<Player> Delete(Guid id)
        {
            Console.WriteLine("Delete called.");
            return _procToUse.Delete(id);
        }
    }
}