using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace assig04
{
    [Route("api/players/{playerId}/items/[controller]")]
    [ApiController]
    public class ItemsController
    {
        private readonly ItemsProcessor _procToUse;

        public ItemsController(ItemsProcessor processor) { _procToUse = processor; }

        [HttpGet]
        [Route("~/api/players/{playerId}/items/Get/{itemId}")]
        public Task<Item> Get(Guid playerId, Guid itemId)
        {
            Console.WriteLine("Get(Item) called.");
            return _procToUse.Get(playerId, itemId);
        }

        [HttpGet]
        [Route("~/api/players/{playerId}/items/GetAll")]
        public Task<Item[]> GetAll(Guid playerId)
        {
            Console.WriteLine("GetAll(Items) called.");
            return _procToUse.GetAll(playerId);
        }

        [HttpPost]
        [Route("~/api/players/{playerId}/items/Create")]
        public Task<Item> Create(Guid playerId, NewItem item)
        {
            Console.WriteLine("Create(Item) called.");
            return _procToUse.Create(playerId, item);
        }

        [HttpPost]
        [Route("~/api/players/{playerId}/items/Modify/{itemId}")]
        public Task<Item> Modify(Guid playerId, Guid itemId, ModifiedItem item)
        {
            Console.WriteLine("Modify(Item) called.");
            return _procToUse.Modify(playerId, itemId, item);
        }

        [HttpDelete]
        [Route("~/api/players/{playerId}/items/Delete/{itemId}")]
        public Task<Item> Delete(Guid playerId, Guid itemId)
        {
            Console.WriteLine("Delete(Item) called.");
            return _procToUse.Delete(playerId, itemId);
        }
    }
}