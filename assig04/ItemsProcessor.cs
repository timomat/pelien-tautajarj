using System;
using System.Threading.Tasks;

namespace assig04
{
    public class ItemsProcessor
    {
        private IRepository _repoToUse;

        public ItemsProcessor(IRepository repository) { _repoToUse = repository; }

        public Task<Item> Get(Guid playerId, Guid itemId) { return _repoToUse.GetItem(playerId, itemId); }

        public Task<Item[]> GetAll(Guid playerId) { return _repoToUse.GetAllItems(playerId); }

        [UnderLeveledExceptionFilter]
        public Task<Item> Create(Guid playerId, NewItem item)
        {
            Item ni = new Item();
            ni.Id = Guid.NewGuid();
            ni.Level = item.Level;
            ni.Type = item.Type;
            Console.WriteLine(item.CreationTime);
            ni.CreationTime = item.CreationTime;
            return _repoToUse.CreateItem(playerId, ni);
        }
        public Task<Item> Modify(Guid playerId, Guid itemId, ModifiedItem item) { return _repoToUse.ModifyItem(playerId, itemId, item); }
        
        public Task<Item> Delete(Guid playerId, Guid itemId) { return _repoToUse.DeleteItem(playerId, itemId); }
    }
}