using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace assig03
{
    public class InMemoryRepository : IRepository
    {
        List<Player> dataBase = new List<Player>();
        public async Task<Player> Get(Guid id)
        {
            Player result = null;

            foreach(Player p in dataBase)
            {
                if(p.Id == id)
                {
                    result = p;
                    break;
                }
            }

            return await Task.FromResult<Player>(result);
        }

        public async Task<Player[]> GetAll()
        {
            return await Task.FromResult<Player[]>(dataBase.ToArray());
        }

        public async Task<Player> Create(Player player)
        {
            dataBase.Add(player);

            return await Task.FromResult<Player>(player);
        }

        public async Task<Player> Modify(Guid id, ModifiedPlayer player)
        {
            Player result = null;

            foreach(Player p in dataBase)
            {
                if(p.Id == id)
                {
                    p.Score = player.Score;
                    p.Level = player.Level;
                    result = p;
                    break;
                }
            }

            return await Task.FromResult<Player>(result);
        }

        public async Task<Player> Delete(Guid id)
        {
            Player result = null;

            foreach(Player p in dataBase)
            {
                if(p.Id == id)
                {
                    result = p;
                    dataBase.Remove(p);
                    break;
                }
            }

            return await Task.FromResult<Player>(result);
        }

        public async Task<Item> GetItem(Guid playerId, Guid itemId)
        {
            Item result = null;

            foreach(Player p in dataBase)
            {
                if(p.Id == playerId)
                {
                    foreach(Item i in p.Items)
                    {
                        if(i.Id == itemId)
                        {
                            result = i;
                            break;
                        }
                    }
                }
            }

            return await Task.FromResult<Item>(result);
        }

        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            foreach(Player p in dataBase)
            {
                if(p.Id == playerId)
                {
                    return await Task.FromResult<Item[]>(p.Items.ToArray());
                }
            }

            return await Task.FromResult<Item[]>(null);
        }

        public async Task<Item> CreateItem(Guid playerId, Item item)
        {
            foreach(Player p in dataBase)
            {
                if(p.Id == playerId)
                {
                    if(item.Type == "Sword" && p.Level < 3) throw new UnderLeveledException();
                    p.Items.Add(item);
                }
            }

            return await Task.FromResult<Item>(item);
        }

        public async Task<Item> ModifyItem(Guid playerId, Guid itemId, ModifiedItem item)
        {
            Item result = null;

            foreach(Player p in dataBase)
            {
                if(p.Id == playerId)
                {
                    foreach(Item i in p.Items)
                    {
                        if(i.Id == itemId)
                        {
                            i.Level = item.Level;
                            i.Type = item.Type;
                            result = i;
                            break;
                        }
                    }
                }
            }

            return await Task.FromResult<Item>(result);
        }

        public async Task<Item> DeleteItem(Guid playerId, Guid itemId)
        {
            Item result = null;

            foreach(Player p in dataBase)
            {
                if(p.Id == playerId)
                {
                    foreach(Item i in p.Items)
                    {
                        if(i.Id == itemId)
                        {
                            result = i;
                            p.Items.Remove(i);
                            break;
                        }
                    }
                }
            }

            return await Task.FromResult<Item>(result);
        }

    }
}