using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace assig03
{
    public class UnderLeveledExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if(context.Exception is UnderLeveledException)
            {
                context.Result = new BadRequestObjectResult("Level is too low for this item.");
            }
        }
    }
}