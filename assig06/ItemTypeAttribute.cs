using System;
using System.Collections.Generic;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace assig06
{
    public class ItemTypeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if(value.ToString() == "Sword" || value.ToString() == "Kebab")
            {
                return ValidationResult.Success;
            }


            return new ValidationResult("Please enter a valid item type.");
        }
    }
}