using System;
using System.Collections.Generic;

namespace assig06
{
    public class Player
    {
        public Player()
        {
            Items = new List<Item>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public bool IsBanned { get; set; }
        public DateTime CreationTime { get; set; }
        public List<Item> Items { get; set; }
        public int Level { get; set; }
    }
}