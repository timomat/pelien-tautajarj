using System;

namespace assig06
{
    public class Item
    {
        public Guid Id { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
        public DateTime CreationTime { get; set; }
    }
}