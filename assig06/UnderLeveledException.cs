using System;

namespace assig06
{
    public class UnderLeveledException : Exception
    {
        public UnderLeveledException() {}
        public UnderLeveledException(string message) : base(message) {}
        public UnderLeveledException(string message, Exception inner) : base(message, inner) {}
    }
}