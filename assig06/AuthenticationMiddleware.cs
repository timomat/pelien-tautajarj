using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace assig06
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            string key = context.Request.Headers["x-api-key"];
            if(key == null) Console.WriteLine("key not found");
            else if(key == "araraa") Console.WriteLine("right key");
            else Console.WriteLine("wrong key");
            await _next(context);
        }
    }
}