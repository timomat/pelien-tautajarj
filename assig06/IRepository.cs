using System;
using System.Threading.Tasks;

namespace assig06
{
    public interface IRepository
    {
         Task<Player> Get(Guid id);
         Task<Player[]> GetAll();
         Task<Player> Create(Player player);
         Task<Player> Modify(Guid id, Player player);
         Task<Player> Delete(Guid id);
         Task<Item> GetItem(Guid playerId, Guid itemId);
         Task<Item[]> GetAllItems(Guid playerId);
         Task<Item> CreateItem(Guid playerId, Item item);
         Task<Item> ModifyItem(Guid playerId, Guid itemId, ModifiedItem item);
         Task<Item> DeleteItem(Guid playerId, Guid itemId);
    }
}