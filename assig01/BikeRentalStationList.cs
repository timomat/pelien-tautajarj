using System.Collections.Generic;

namespace assig01
{
    public class BikeRentalStationList
    {
        public List<Stations> stations;

        public class Stations
        {
            public int id;
            public string name;
            public double x;
            public double y;
            public int bikesAvailable;
            public int spacesAvailable;
            public bool allowDropoff;
            public bool isFloatingBike;
            public bool isCarStation;
            public string state;
            public List<string> networks;
            public bool realTimeData;
        }
        
    }
}