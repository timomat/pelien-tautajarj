using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http;

namespace assig01
{
    public class RealTimeCityBikeDataFetcher : ICityBikeDataFetcher
    {
        HttpClient client = new HttpClient();
        public async Task<int> GetBikeCountInStation(string stationName) {
            foreach(char c in stationName)
            {
                if(Char.IsDigit(c)) throw new ArgumentException("No numbers allowed.");
            }
            
            int result = -1;
            var bikeStationString = await client.GetStringAsync("http://api.digitransit.fi/routing/v1/routers/hsl/bike_rental");
            BikeRentalStationList jsonList = JsonConvert.DeserializeObject<BikeRentalStationList>(bikeStationString);
            foreach(BikeRentalStationList.Stations s in jsonList.stations)
            {
                if(s.name == stationName)
                {
                    result = s.bikesAvailable;
                    break;
                }
            }
            
            if(result < 0) throw new NotFoundException(stationName);

            return result;
        }
    }
}