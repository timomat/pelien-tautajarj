using System.Threading.Tasks;
using System.Collections.Generic;

namespace assig01
{
    public interface ICityBikeDataFetcher
    {
        Task<int> GetBikeCountInStation(string stationName);
    }
}