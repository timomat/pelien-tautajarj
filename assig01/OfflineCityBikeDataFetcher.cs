using System.Threading.Tasks;
using System;

namespace assig01
{
    public class OfflineCityBikeDataFetcher : ICityBikeDataFetcher
    {
        public async Task<int> GetBikeCountInStation(string stationName)
        {
            int result = -1;
            string[] stations = await System.IO.File.ReadAllLinesAsync("bikedata.txt");
            foreach(string s in stations)
            {
                string[] parts = s.Split(':');
                if(parts[0].Contains(stationName))
                {
                    result = Int32.Parse(parts[1]);
                    break;
                }
            }

            if(result < 0) throw new NotFoundException(stationName);
            
            return result;
        }
    }
}