﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace assig01
{
    class Program
    {
        static void Main(string[] args)
        {
            try {
                ICityBikeDataFetcher fetch = null;

                if(args[1] == "realtime") fetch = new RealTimeCityBikeDataFetcher();
                else if(args[1] == "offline") fetch = new OfflineCityBikeDataFetcher();
                else throw new ArgumentException("Second argument must be 'realtime' or 'offline'.");
                
                Console.WriteLine("Searching for: " + args[0]);

                Task<int> result = fetch.GetBikeCountInStation(args[0]);
                result.Wait();
                Console.WriteLine("Available bikes: " + result.Result);

            } catch(NotFoundException e) {
                Console.WriteLine("Not found:" + e.Message);

            } catch(Exception e) {
                Console.WriteLine(e.Message);

            }
        }
    }
}
