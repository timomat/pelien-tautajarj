using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;

namespace project
{
    public class MongoDbRepository : IRepository 
    {
        private readonly IMongoCollection<Match> _collection;
        private readonly IMongoCollection<BsonDocument> _bsonDocumentCollection;

        public MongoDbRepository()
        {
            MongoClient client = new MongoClient("mongodb://localhost:27017");
            IMongoDatabase db = client.GetDatabase("tictactoe");
            _collection = db.GetCollection<Match>("matches");
            _bsonDocumentCollection = db.GetCollection<BsonDocument>("matches");
        }

        public async Task<Match> GetMatch(Guid matchId)
        {
            FilterDefinition<Match> filter = Builders<Match>.Filter.Eq("_id", matchId);
            return await _collection.Find(filter).FirstAsync();
        }
        public async Task<Match[]> GetAllMatches()
        {
            List<Match> matches = await _collection.Find(new BsonDocument()).ToListAsync();
            return matches.ToArray();
        }
        public async Task<Match> CreateMatch(Match match)
        {
            await _collection.InsertOneAsync(match);
            return match;
        }
        public async Task<Match> UpdateMatch(Guid matchId, Match match)
        {
            FilterDefinition<Match> filter = Builders<Match>.Filter.Eq("_id", matchId);
            await _collection.ReplaceOneAsync(filter, match);
            return match;
        }
        public async Task<Match> DeleteMatch(Guid matchId)
        {
            FilterDefinition<Match> filter = Builders<Match>.Filter.Eq("_id", matchId);
            Match deleted = await _collection.Find(filter).FirstAsync();
            await _collection.DeleteOneAsync(filter);
            return deleted;
        }
    }
}