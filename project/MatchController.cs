using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace project
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchController
    {
        private readonly MatchProcessor _matchProcessor;
        public MatchController(MatchProcessor processor) { _matchProcessor = processor; }

        [HttpGet]
        [Route("~/api/GetMatch/{matchId}")]
        public Task<Match> GetMatch(Guid matchId) { return _matchProcessor.GetMatch(matchId); }

        [HttpGet]
        [Route("~/api/GetAllMatches")]
        public Task<Match[]> GetAllMatches() { return _matchProcessor.GetAllMatches(); }

        [HttpGet]
        [Route("~/api/CreateMatch")]
        public Task<Match> CreateMatch() { return _matchProcessor.CreateMatch(); }

        [HttpPost]
        [Route("~/api/UpdateMatch/{matchId}")]
        public Task<Match> UpdateMatch(Guid matchId, Move move) { return _matchProcessor.UpdateMatch(matchId, move); }

        [HttpDelete]
        [Route("~/api/DeleteMatch/{matchId}")]
        public Task<Match> DeleteMatch(Guid matchId) { return _matchProcessor.DeleteMatch(matchId); }
    }
}