using System;
using System.Collections.Generic;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace project
{
    public class RowValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if( ((int)value) > 0 && ((int)value) < 4)
            {
                return ValidationResult.Success;
            }


            return new ValidationResult("Row must be '1', '2' or '3'.");
        }
    }
}