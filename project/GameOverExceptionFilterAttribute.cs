using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace project
{
    public class GameOverExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if(context.Exception is GameOverException)
            {
                context.Result = new BadRequestObjectResult("This game has already ended, no more moves allowed.");
            }
        }
    }
}