using System;
using System.Collections.Generic;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace project
{
    public class SideValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if( ((string)value) == "cross" || ((string)value) == "circle")
            {
                return ValidationResult.Success;
            }


            return new ValidationResult("Side must be either 'cross' or 'circle'.");
        }
    }
}