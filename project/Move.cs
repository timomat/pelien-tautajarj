using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace project
{
    public class Move
    {
        [Required]
        [SideValidate]
        public string side {get; set;}

        [Required]
        [ColumnValidate]
        public char column {get; set;}

        [Required]
        [RowValidate]
        public int row {get; set;}
    }
}