using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace project
{
    public class NotYourTurnExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if(context.Exception is NotYourTurnException)
            {
                context.Result = new BadRequestObjectResult("It is not your turn, wait for the other player.");
            }
        }
    }
}