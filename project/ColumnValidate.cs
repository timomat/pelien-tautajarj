using System;
using System.Collections.Generic;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace project
{
    public class ColumnValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if( ((char)value) == 'a' || ((char)value) == 'b' || ((char)value) == 'c')
            {
                return ValidationResult.Success;
            }


            return new ValidationResult("Column must be 'a', 'b' or 'c'.");
        }
    }
}