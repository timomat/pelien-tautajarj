using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace project
{
    public class InvalidMoveExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if(context.Exception is InvalidMoveException)
            {
                context.Result = new BadRequestObjectResult("This move is invalid, that cell is already filled.");
            }
        }
    }
}