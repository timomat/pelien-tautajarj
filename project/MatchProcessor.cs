using System;
using System.Threading.Tasks;

namespace project
{
    public class MatchProcessor
    {
        private IRepository _matchRepository;
        public MatchProcessor(IRepository repository) { _matchRepository = repository; }
        public Task<Match> GetMatch(Guid matchId) { return _matchRepository.GetMatch(matchId); }
        public Task<Match[]> GetAllMatches() { return _matchRepository.GetAllMatches(); }
        public Task<Match> CreateMatch()
        {
            Match match = new Match();
            return _matchRepository.CreateMatch(match);
        }

        [GameOverExceptionFilter]
        [NotYourTurnExceptionFilter]
        [InvalidMoveExceptionFilter]
        public Task<Match> UpdateMatch(Guid matchId, Move move)
        {
            Match match = GetMatch(matchId).Result;

            if(!match.onGoing) throw new GameOverException();

            if((move.side == "circle" && match.turn == 1) || (move.side == "cross" && match.turn == -1)) throw new NotYourTurnException();

            int row = move.row-1, column = 0;
            switch(Char.ToLower(move.column))
            {
                case 'a':
                    column = 0;
                    break;
                case 'b':
                    column = 1;
                    break;
                case 'c':
                    column = 2;
                    break;
            }
            if(match.board[row,column] != 0) throw new InvalidMoveException();

            match.board[row,column] = match.turn;
            match.turn *= -1;
            
            int result = CheckForResult(match.board);
            if(result != 0)
            {
                match.onGoing = false;
                match.result = result;
            }

            return _matchRepository.UpdateMatch(matchId, match);
        }
        public Task<Match> DeleteMatch(Guid matchId) { return _matchRepository.DeleteMatch(matchId); }

        private int CheckForResult(int[,] board)
        {
            //Rows
            if(board[0,0] != 0 && board[0,0] == board[0,1] && board[0,1] == board[0,2]) return board[0,0];
            else if(board[1,0] != 0 && board[1,0] == board[1,1] && board[1,1] == board[1,2]) return board[1,0];
            else if(board[2,0] != 0 && board[2,0] == board[2,1] && board[2,1] == board[2,2]) return board[2,0];
            //Columns
            else if(board[0,0] != 0 && board[0,0] == board[1,0] && board[1,0] == board[2,0]) return board[0,0];
            else if(board[0,1] != 0 && board[0,1] == board[1,1] && board[1,1] == board[2,1]) return board[0,1];
            else if(board[0,2] != 0 && board[0,2] == board[1,2] && board[1,2] == board[2,2]) return board[0,2];
            //Diagonals
            else if(board[0,0] != 0 && board[0,0] == board[1,1] && board[1,1] == board[2,2]) return board[0,0];
            else if(board[2,0] != 0 && board[2,0] == board[1,1] && board[1,1] == board[0,2]) return board[2,0];
            //Draw
            else
            {
                foreach(int i in board)
                {
                    if(i == 0) return 0;
                }
                return 2;
            }
        }
    }
}