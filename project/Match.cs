using System;

namespace project
{
    public class Match
    {
        public Match()
        {
            onGoing = true;
            turn = 1;
            board = new int[3,3];
            result = 0;
        }

        public Guid id;
        public bool onGoing;
        public int turn;
        public int[,] board;
        public int result;
    }
}