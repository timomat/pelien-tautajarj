using System;

namespace project
{
    public class NotYourTurnException : Exception
    {
        public NotYourTurnException() {}
        public NotYourTurnException(string message) : base(message) {}
        public NotYourTurnException(string message, Exception inner) : base(message, inner) {}
    }
}