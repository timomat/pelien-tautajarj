using System;
using System.Threading.Tasks;

namespace project
{
    public interface IRepository
    {
         Task<Match> GetMatch(Guid matchId);
         Task<Match[]> GetAllMatches();
         Task<Match> CreateMatch(Match match);
         Task<Match> UpdateMatch(Guid matchId, Match match);
         Task<Match> DeleteMatch(Guid matchId);
    }
}